#WEBPOS Docker

##Building it
1. Create a file named, `.env` in the same directory as `docker-compose.yml`. You can use `.env.sample` as a starting point, by copying it and renaming it to `.env`.
2. Add an env variable to `.env`: `EWINERY_ROOT=/User/..../Code/git` which should lead to the root directory of your installation of the ewinery code. DO NOT commit this file to git.
3. cd ./lucee
4. docker build -t mposapi-server .

Tomcat's server.xml may need this?
```
<Host name="mpos.local" appBase="webapps">
    <Context path="" docBase="/var/www" />
    <Alias>mpos.local</Alias>
</Host>
```

##Running it
1. `cd` to the root: `./docker-mpos-api`
2. Run `docker-compose up -d`
3. Enter `http://localhost:8888/mojoe/main/index.cfm?method=getSession` into the browser's location bar
4. Install Postman, a desktop WEBAPI console for better interactions with the API, especially when POSTing JSON data payloads.

Note: TLS isn't working yet.


##Helpful Commands

| Command      | Benefits |
| ------------ | ----------- |
| `docker container ls`      | show all *running* containers you have        |
| `docker image ls`   | show all images you have        |
| `docker image rm d1aa3eb6420c` | remove an image identified by d1aa3eb6420c |
| `docker-compose up` | create and start containers |
| `docker-compose down` | stop the containers |
| `docker-compose config` | prints out resolved configs, including values from the .env files |
| `docker-compose rm` | remove containers |
| `docker start` | start previously made containers (containers must exist)
| `docker stop` | |
| `docker pause` | | 
| `docker unpause` |  |
