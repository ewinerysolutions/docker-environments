#!/bin/bash

docker run -dit -p 8080:80 -v "./vinsuite-pos-tabletapp/platforms/browser/www:/usr/local/apache2/htdocs/app" --rm --name mpos.localhost --hostname mpos.localhost webpos1-server

# add --rm flag??
#
# docker run -dit -p 8080:80 --mount type=bind,source=/Users/mgatto/Development/vinsuite/Applications/vinsuite-pos-tabletapp/platforms/browser/www,target=/usr/local/apache2/htdocs/app webpos1-server
