#WEBPOS Docker

##Building it
1. cd ./docker-mpos-local
2. docker build -t webpos1-server .

##Running it
1. cd ./docker-mpos-local
2. docker run -dit -p 8080:80 -p 443:443 -v "/full/path/to/WEBPOS/code/vinsuite-pos-tabletapp/platforms/browser/www:/usr/local/apache2/htdocs/app" --rm --name mpos.localhost  --hostname mpos.localhost webpos1-server
3. enter `https://mpos.localhost/app/index.html` into the browser's location bar

##Helpful Commands


| Command      | Benefits |
| ------------ | ----------- |
| `docker container ls`      | show all *running* containers you have        |
| `docker image ls`   | show all images you have        |
| `docker image rm d1aa3eb6420c` | remove an image identified by d1aa3eb6420c |

